db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$count: "fruitsOnSale"
	}
]);

db.fruits.aggregate([
	{
		$match: { stock: { $gte: 20 } }
	},
	{
		$count: "enoughStock"
	}
]);

db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplierId",
			avgPrice: { $avg: "$price" }
		}
	}
]);

db.fruits.aggregate([
	{
		$group: {
			_id: "$supplierId",
			maxPrice: { $max: "$price" }
		}
	}
]);

db.fruits.aggregate([
	{
		$group: {
			_id: "$supplierId",
			minPrice: { $min: "$price" }
		}
	}
]);
